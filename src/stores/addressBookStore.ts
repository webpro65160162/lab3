import { ref} from 'vue'
import { defineStore } from 'pinia'

interface AddressBook{
  id: number
  name: string
  tel: string
  gender: string
}

export const useAddressBookStore = defineStore('address_book', () => {

let lastId = 1
const address = ref<AddressBook>({
  id: 0,
  name: '',
  tel: '',
  gender: 'Male'
})

const addressList = ref<AddressBook[]>([])
const isAddNew = ref(false)

function save() {
  //address.value.id = lastId++
  if(address.value.id>0) {
    //Edit
  const editedIndex = addressList.value.findIndex((item) => item.id === address.value.id)
  addressList.value[editedIndex] = address.value

  } else { //Add
    addressList.value.push({...address.value, id: lastId++})
  }
  isAddNew.value = false
  address.value = {
    id: 0,
    name: '',
    tel: '',
    gender: 'Male'
  }
}

function edit(id: number) {
  isAddNew.value = true
  const editedIndex = addressList.value.findIndex((item) => item.id ===id)
  //Copy Odject JSON.parse(JSON.stringify(Object))
  address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex]))
}

function remove(id: number) {
  const removeIndex = addressList.value.findIndex((item) => item.id ===id)
  addressList.value.splice(removeIndex, 1)
}

function cancel() {
  isAddNew.value = false
  address.value = {
    id: 0,
    name: '',
    tel: '',
    gender: 'Male'
  }
}

  return { address, addressList, save, isAddNew, edit, remove, cancel}
})
